[[octopusConfig]]

This chapter describes all the configuration options of the Octopus framework.

By default, the +octopusConfig.properties+ file is read on the classpath. But we can specify another properties file with a JVM system property so that we can configure the WAR file externally (no need to rebuild the WAR file between environment)

With the -Doctopus.cfg=<someURL> option, we can specify the location of an additional properties file which will be read and use for configuring the system.

=== Configuration properties

==== securedURLs.file

default : */WEB-INF/securedURLs.ini*

The securedURLs.ini file contains the permissions required to access some URLs.  See ?? for the format of the file. The file can be optional since v0.9.7.

The entries can also be defined in a programmatic way. A CDI bean implementing +be.c4j.ee.security.url.ProgrammaticURLProtectionProvider+ will be used to define additional entries.

The key value of the Map must contain the URL pattern, the value is the filter.

These entries will be placed after the values defined in the file (order is important)

==== namedPermission.class

default : *(none)*

Defines the Enum class which enumerates all permissions. Within the demo example it is the class *be.c4j.demo.security.permission.DemoPermission*.

==== namedPermissionCheck.class

default : *(none)*

Defines the annotation which can be used on method and class level to define the security requirements.

==== namedRole.class

default : *(none)*

Defines the Enum class which enumerates all named roles. It is the role counterpart of the namedPermission.class configuration option.

==== namedRoleCheck.class

default : *(none)*

Defines the annotations which can be used on method and class level to define the security requirements.

==== additionalShiroIniFileNames

default : *classpath:shiro_extra.ini*

Define the file where we can customize Shiro directly. This file will be merged with the config of octopus. Multiple files can be specified by separating them by a ,.

==== globalAuditActive

default : *false*

When true, each server request will result in a CDI event with payload OctopusAuditEvent.

==== show.debug

default : *(none)*

Defines the type of debug information which needs to be logged. These are the supported values (multiple values can be specified with comma (,) separation)
*INI*: Log the dynamically generated Shiro ini file.
*SSO_FLOW*: Prints out the basic steps within the SSO flow between Client and Server.
*SSO_REST*: Prints out the contents of the Rest calls between the SSO Client and SSO Server.

==== aliasNameLoginBean (JSF Only)

default : *(none)*

The CDI managed bean which can be used to login and logout the user is called *loginBean*. With this configuration option, you can give it an additional name so that you can use this configured value in the JSF pages instead of *loginBean*.

==== loginPage (JSF Only)

default : */login.xhtml*

The JSF page which will be called when the user access a security restricted page in your application and isn't logged in yet.  It should contain the fields and button to allow him to login into your application.

==== logoutPage (JSF Only)

default : */*

URL used as redirect after the local session is logged out. When the value starts with a '/' the URL is relative to the root, otherwise, it should a full address.

==== unauthorizedExceptionPage (JSF Only)

default : */unauthorized.xhtml*

The JSF page which is called when the user access a security restricted page and he doesn't has the required permissions (roles) to access it.

==== allowPostAsSavedRequest (JSF Only)

default : *true*

When the user is redirected to the login screen because he is not authenticated yet, the original URL is stored.  So it can be used to redirect to if the user has supplied valid credentials.  However, with JSF applications, posting to an arbitrary page, results in exceptions as the state of the previous pages is missing.

With this property, you can disable the redirect to an URL which uses POST as HTTP method.  A redirect to the welcome page of your application will be used instead.

==== session.hijacking.level (JSF Only)

default : *ON*

Determines the Session Hijack Protection level. It uses the IP Address and User-Agent header information and checks if the sessionId could be 'stolen'.

The default level _ON_, checks both properties, _PARTIAL_ only the User-Agent header value and _OFF_ disables the protection.

==== session.single (JSF Only)

default : *true*

When active, only one session for a user is allowed (based on the principalId defined in the AuthenticationInfoBuilder). When the user logs on, the other session is automatically logged out.

==== primefaces.mobile.exclusion (JSF Only)

default : *false*

When true, the Mobile Renderers of primeFaces are excluded. This can be needed due to an issue which occurred in a certain situation but could not be reproduced in other situations.

==== hashAlgorithmName

default : *(none)*

Name of the MessageDigest algorithm when you use hashed passwords. examples are Md5 and Sha512.

==== saltLength

default : *0*

Number of bytes used when creating a salt for the hashing of passwords.  0 means that no salt is used.

==== hashEncoding

default : *HEX*

Defines how the hashed passwords are encoded (HEX or BASE64) before they are compared to the supplied value which should be identically before access is granted. The value specified in the configuration file is case insensitive compared with the allowed values.

==== cacheManager.class

default : *org.apache.shiro.cache.MemoryConstrainedCacheManager*

The class responsible for holding/managing the cache of the authentication and authorization data.  The developer can supply a custom implementation of +org.apache.shiro.cache.AbstractCacheManager+ when the cache needs different logic or storage location.

==== session.invalidate.login

default : *true*

Defines if the session available during login (username - password, OAuth2, KeyCloak, CAS, ... but not the authenticating filters mostly used for JAX-RS endpoints) needs to be invalidated.

This means that the user gets a new session id after login. HTTPSession attributes are copied to the new session.

==== jwt.algorithms (JWT User Server and JWT User Client Only)

default : *(none)*

Requirements are different based on the module which uses the value.

The parameter must contain a JWT Signature algorithm with the  JWT User Client module. The value is ignored in the server module.

Valid values for the signature definition are HS256, HS384 and HS512.

The parameter may contain a way of encryption of you also want to have the JWT with user information encrypted. It that case, definition must be done with the JWT User Server and JWT User Client module.

Valid values are AES, EC and RSA.

There are also other parameter required and encryption works only with the JCE installed.

==== jwt.hmac.secret (JWT User Server and JWT User Client Only)

default : *(none)*

Required value. Used as a the secret for the HMAC calculation of the JWT signature algorithm.

Based on the selected algorithm (see jwt.algorithms ) the length requirement can be different.

==== jwt.aes.secret (JWT User Server and JWT User Client Only)

default : *(none)*

Required value when the _jwt_algorithms_ parameter contains the value *AES*. It will be used as the secret key for the AES encrryption and decryption.

The value is Base64 decoded first.

==== jwk.file (JWT User Server and JWT User Client Only)

default : *(none)*

Required value when the _jwt_algorithms_ parameter contains the value *EC* or *RSA*. It needs to indicate a JWK file location (relative and absolute path are allowed) containing the private or public part of the key for the encryption / decryption.

==== jwt.token.timeToLive (JWT User Client only)

default : *2*

The number of seconds the generated JWT will be valid.

==== OAuth2.clientId (OAuth2 only)

default : *(none)*

The value used for the clientId configuration value towards your OAuth2 provider.

==== OAuth2.secretId (OAuth2 only)

default : *(none)*

The value used for the secretId configuration value towards your OAuth2 provider.

==== OAuth2.scopes (OAuth2 only)

default : *(none)*

The additional scopes you want to use when the authentication is performed with the OAuth2 Provider.

==== OAuth2.provider.selectionPage (OAuth2 only)

default : */login.xhtml*

The page which is shown to the user when multiple OAuth2 providers are found on the classpath so that the user can choose which provider he wants to take.

==== keycloak.file (keycloak only)

default : *(none)*

The location of the JSON configuration file for the Keycloak integration. It can be generated by using the Keycloak Web admin pages.

==== keycloak.scopes (keycloak only)

default : *(none)*

Additional scopes you want to pass to Keycloak. Std OpenIdConnect feature.

==== keycloak.idpHint (keycloak only)

default : *(none)*

When multiple Social Login providers are configured, hint the user to a specific one.

==== keycloak.single.logout (keycloak only)

default : *true*

Is Single logout active? When true, the Keycloak server is called to end the SSO session for the user. In turn, Keycloak will contact all applications which are using the SSO session to terminate their specific session.

==== SSO.server (Octopus SSO, CAS, SAML, Keycloak)

default : *(none)*

The login URL (or part of it) of the remote authentication page. See the specific authentication module for correct usage.

==== SSO.cookie.name (Octopus SSO Server)

default : *OctopusSSOToken*

The name of the cookie contain the SSO token (allows an authentication without entering username / password for example when valid cookie found)

==== SSO.cookie.timetolive (Octopus SSO Server)

default : *10h*

The time the SSO cookie is active. By default, it is 10 hours. Valid time units are h -> Hours, d -> Days and m -> Months (=30 days)

==== SSO.cookie.secure (Octopus SSO Server)

default : *true*

Is the secure flag on the cookie set, meaning it is only added to https requests.

==== CAS.protocol (CAS only)

default : *CAS*

The protocol used with the CAS server for exchange of authentication. Other supported value is SAML.

==== CAS.single.logout (CAS only)

default : *true*

Is Single logout active? When true, after local logout the browser is redirected to the logout page of CAS resulting in ending the SSO session and all Local sessions active under that SSO Session.

==== CAS.property.email (CAS only)

default : *email*

The name of the CAS attribute containing the email address.

==== jwk.file (JWT only)

default : *(none)*

Location of the JWK file (with RSA public keys) for the JWT signature verification. See ???

==== jwt.systemaccounts.only (JWT only)

default : *True*

Are only SystemAccounts allowed when using authentication based on JWT tokens?

==== jwt.systemaccounts.map (JWT only)

Properties file where the system accounts are defined for each api-key.

==== fakeLogin.localhostOnly (fakeLogin only)

default : *true*

When using offline login authentication instead of OAuth2, is this only allowed on localhost?

==== SSO.application (Octopus SSO only)

default : *(none)*

Future usage when Octopus SSO is fully operational. ??? Document this config property.

==== SSO.application.suffix (Octopus SSO only)

default : *(none)*

Future usage when Octopus SSO is fully operational. ??? Document this config property.