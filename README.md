
Permission based security for Java EE application

+ Permission based framework for Java EE
+ Secures URL, JSF components and CDI and EJB method calls
+ Very flexible, can be easily integrated within your application
+ Tightly integrated with CDI
+ Type-safe definition of permissions
+ Declarative declaration of JSF security (with tags, not using rendered attribute)
+ Support for salted hashed passwords and remember me functionality
+ Custom voter can be created for more complex security requirements